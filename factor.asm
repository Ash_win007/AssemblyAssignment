extern printf
extern scanf

section .data
	out: db "Enter the number " , 10, 0
	in: dd 0
	f: db "%d" , 0
	s: db "%s" , 0
	o: dd 0
section .text


	global main
main:
	push ebp
	mov ebp , esp
	push out
	push s
	call printf
	
	push in
	push f
	call scanf
	mov dword [o] , 0
	mov ebx , dword [o]
	mov eax , dword [in]
l:
	add ebx , 1
	idiv ebx
	cmp edx , 0
	je l2
	
	cmp eax , ebx
	jne l
	je l3
l2:
	push ebx
	push f
	
	call printf
l3:
	mov esp , ebp
	pop ebp		
	ret
