extern printf
extern scanf

section .data
	out: db "Number of numbers please" ,10 , 0
	in: dd  0
	f: db "%d" , 0
	sum: dd  0
	d: db "%s" , 0
	t: db "Sum of numbers" , 10, 0
section .text

	global main
main:
	push ebp
	mov ebp , esp
	push out
	push d
	call printf
	push in
	push f
	call scanf
	push t
	push d
	call printf
l:	
	mov ebx , dword [in]
	add dword [sum] , ebx
	sub dword [in] , 1
	cmp dword [in] , 0
	jne l

	push sum
	push f
	call printf

	mov esp , ebp
	pop ebp
	ret
