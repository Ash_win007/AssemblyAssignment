extern printf
extern scanf

section .data
	out: db "Hey there! %s" ,10,0
	i: db "%s" ,0
	p: db 0
section .text

	global main
main:
	push ebp
	mov ebp , esp
	push p
	push i
	call scanf
	push p
	push out
	call printf
	mov esp , ebp
	pop ebp
	ret
